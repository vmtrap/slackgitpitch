
![LOGO SLACK](./img/Slack_RGB.svg)

<span style="color:gray">Cloud-based team collaboration tool</span>

---

### Slack
<span style="color: #e49436; text-transform: none">S</span>earchable
<span style="color: #e49436; text-transform: none">L</span>og of
<span style="color: #e49436; text-transform: none">A</span>ll
<span style="color: #e49436; text-transform: none">C</span>onversation and
<span style="color: #e49436; text-transform: none">K</span>nowledge

+++

Historically based on <span style="color: #e49436; text-transform: none">IRC</span>, so IRC-like features:

- persistent channels (chat rooms)
- private groups
- direct messaging
- etc.

+++

All content is <span style="color: #e49436; text-transform: none">searchable</span>:

- files
- people
- conversations
- etc.

+++

Allow integration of <span style="color: #e49436; text-transform: none">third-party services</span>:

- Github
- Google Drive
- Trello
- Dropbox
- etc.

---

### ADLab current slack

[compbio-ulaval.slack.com](https://compbio-ulaval.slack.com/)

---

### Pricing & plans

+++

-|Free|Standard|Plus
--|--|--|--
Price: /user/month|$0|$8|$15
Price: /user/month<br/>billed annually|$0|$6.67|$12.5

Not involved: <span style="color:#666666;text-transform:none">Slack Enterprise Grid</span>

+++

#### Free plan features

- <span style="color:#e49436;text-transform:none">Searchable</span> message archives
- Max: <span style="color: #e49436; text-transform: none">10k</span> most recent messages
- <span style="color: #e49436; text-transform: none">10</span> apps or service integrations
- Native apps for <span style="color: #e49436; text-transform:none">iOS</span>, <span style="color: #e49436; text-transform: none">Android</span>, <span style="color: #e49436; text-transform: none">Mac</span> & <span style="color: #e49436; text-transform: none">Windows desktop</span>
- <span style="color: #e49436; text-transform: none">Two-person</span> voice & video calls
- <span style="color: #e49436; text-transform: none">5GB total file storage</span> for the team

+++

#### Standard plan features

- <span style="color:#e49436;text-transform:none;">All free plan features</span>
- <span style="color:#e49436;text-transform:none">Unlimited</span> searchable message archives
- <span style="color:#e49436;text-transform:none">Unlimited</span> apps and service integrations
- OAuth via Google
- Custom User Groups to reach a team or department
- <span style="color:#e49436;text-transform:none">Group</span> voice and video calls
- <span style="color:#e49436;text-transform:none">10GB file storage</span> per team member
- Custom Profiles
- Some more...

+++

#### Plus plan features

- <span style="color:#e49436;text-transform:none;">All standard plan features</span>
- SAML-based single sign-on (SSO)
- Compliance Exports of all messages
- <span style="color:#e49436;text-transform:none;">99.99% Guaranteed</span> Uptime SLA
- 24/7 Support with 4-hour response time
- User provisioning and deprovisioning
- Real-time Active Directory sync with OneLogin, Okta, Centrify, and Ping Identity
- <span style="color:#e49436;text-transform:none">20GB file storage</span> per team member

---

### View

+++

![slack view](img/slack_example.jpg)

+++

![slack view](img/mattermost_menu.png)

---

### Alternatives

+++

MS Office 365<br/>Teams|Altassian<br/>HipChat|Mattermost, Inc<br/>Mattermost
--|--|--
![logo Microsoft Teams](img/microsoft_teams_logo.png)|<img alt="logo hipchat" src="https://bytebucket.org/vmtrap/slackgitpitch/raw/01007fd161dc370478bb998149ba5d269ce75b2a/img/hipchat_logo.svg" width="512px"/>|<img alt="logo mattermost" src="https://bytebucket.org/vmtrap/slackgitpitch/raw/01007fd161dc370478bb998149ba5d269ce75b2a/img/mattermost_logo.svg" width="512px"/>
[Plan (Business)](https://products.office.com/en-us/compare-all-microsoft-office-products?tab=1)|[Plan](https://www.hipchat.com/pricing)|[Plan](https://about.mattermost.com/pricing/)

---

### Demo

[adtestworld.slack.com](https://adtestworld.slack.com)

---

### Conclusion
